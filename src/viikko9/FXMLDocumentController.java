/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.io.IOException;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Aleksi
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Button addMoneyButton;
    @FXML
    private Button buyButton;
    @FXML
    private Button emptyButton;
    @FXML
    private Button printButton;
    @FXML
    private Slider moneySlider;
    @FXML
    private Label moneyLabel;
    @FXML
    private ListView<Bottle> bottleList;
    @FXML
    private Label consoleLabel;
    @FXML
    private ComboBox<String> typeCombo;
    @FXML
    private ComboBox<Double> sizeCombo;
    @FXML
    private Button receiptButton;
    
    //Singleton principle, creating bottle dispenser instance
    BottleDispenser bd = BottleDispenser.getInstance();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //Create values for comboboxes
        sizeCombo.getItems().addAll(0.5,1.5);
        typeCombo.getItems().addAll("Pepsi Max","Coca-Cola Zero","Fanta Zero");
        
        //Create bottle list
        bottleList.getItems().addAll(bd.getArray());
        
    }    

    @FXML
    private void addMoney(ActionEvent event) {
        
        //Rounding money value to nearest 0,05
        double money = moneySlider.getValue();
        DecimalFormat df = new DecimalFormat("#0.00");
        df.setRoundingMode(RoundingMode.CEILING);
        money = Double.parseDouble(df.format(money).replace(",","."));
        money = Math.round(money * 20.0)/20.0;
        
        //Original bd function to add money to dispenser
        bd.addMoney(money);
        
        //Set money label to show existing money amount in machine
        moneyLabel.setText(df.format(bd.getMoney()) + "€");

        //Set money slider to 0
        moneySlider.setValue(0);
        //Set button text to default
        addMoneyButton.setText("Lisää rahaa koneeseen");
    }

    @FXML
    private void buyBottle(ActionEvent event) {
           
        //If neither type or size of bottle is null in GUI
        if(typeCombo.valueProperty().getValue() != null 
                && sizeCombo.valueProperty().getValue() != null
                && bd.getArray().size() > 0){

            //Going through all items in ArrayList
            for(int i = 0;i < bd.getArray().size();i++){

                Bottle bottle;
                bottle = (Bottle) bd.getArray().get(i);
                double size = bottle.getVolume();
                double price = bottle.getPrice();
                String type = bottle.getName();

                //If type and size match, do purchase
                if(type == typeCombo.valueProperty().getValue() 
                        && size == sizeCombo.valueProperty().getValue()){

                        consoleLabel.setText(bd.buyBottle(i));

                        //String money = Double.toString(Math.round(bd.getMoney() * 20.0)/20.0);
                        
                        DecimalFormat df = new DecimalFormat("#0.00");

                        moneyLabel.setText(df.format(Math.round(bd.getMoney() * 20.0)/20.0) + "€");
                        
                        //Update bottle list
                        bottleList.getItems().clear();
                        bottleList.getItems().addAll(bd.getArray());
                        break;
                }
                //If no hits can be found on bottle list, print error message
                else{ 
                    consoleLabel.setText("Valitsemaasi pulloa ei ole saatavilla.");
                }
            }
        }
        //If no bottles left, print error message
        else if(bd.getArray().size() == 0){ 
            consoleLabel.setText("Automaatti on tyhjä.");
        }
        //If type or size is null, print error message
        else{ 
            consoleLabel.setText("Virheellinen valinta.");
        }
    }
        

    @FXML
    private void returnMoney(ActionEvent event) {
        
        //Rounding money value to nearest 0,05
        double money = bd.getMoney();
        DecimalFormat df = new DecimalFormat("#0.00");
        df.setRoundingMode(RoundingMode.CEILING);
        money = Double.parseDouble(df.format(money).replace(",","."));
        money = Math.round(money * 20.0)/20.0;
        
        //Original function to return money
        bd.returnMoney();
        
        //Prints copied from original function
        if(money > 0){
            consoleLabel.setText("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + moneyLabel.getText());
            moneyLabel.setText("0,00€");
        }
        //If user tries to return money from empty machine, print error message
        else{
            consoleLabel.setText("Masiinassa ei ole rahaa.");
        }
    }

    
    @FXML // Currently obsolete function as trigger button is hidden
    private void printBottles(ActionEvent event) {

        bottleList.getItems().clear();
        bottleList.getItems().addAll(bd.getArray());
        
    }

    @FXML
    private void setMoneyButtonValue(MouseEvent event) {
        
        //Rounding money value to nearest 0,05
        double money = moneySlider.getValue();
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.CEILING);
        money = Math.round(money * 20.0)/20.0;
        
        addMoneyButton.setText("Lisää koneeseen " + df.format(money) + "€");
 
    }

    private void setMoneyButtonValue(DragEvent event) {
        
        //Rounding money value to nearest 0,05
        double money = moneySlider.getValue();
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        money = Math.round(money * 20.0)/20.0;
        
        //Rules for printing resulting sum to get 2 decimals
        if(df.format(money).length() >= 4){
            addMoneyButton.setText("Lisää koneeseen " + df.format(money) + "€");
        }
        else if(df.format(money).length() == 3){
            addMoneyButton.setText("Lisää koneeseen " + df.format(money) + "0€");
        }
        else{
            addMoneyButton.setText("Lisää koneeseen " + df.format(money) + ",00€");
        }
    }

    @FXML
    private void printReceipt(ActionEvent event) throws IOException {
        bd.printReceipt();
    }
}
