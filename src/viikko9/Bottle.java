/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

/**
 *
 * @author Aleksi
 */
public class Bottle {
    private String name;
    private String manufacturer;
    private double volume;
    private double price;
    private int id;
    static private int bottleCount;
    
    public Bottle(){
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        volume = 0.5;
        price = 1.80;
        id = bottleCount;
        bottleCount++;
    }
    
    public Bottle(String n, String manuf, double vol, double p){
        name = n;
        manufacturer = manuf;
        volume = vol;
        price = p;
        id = bottleCount;
        bottleCount++;
}
    
    public String getName(){
        return this.name;
    }
    
    public String getManufacturer(){
        return this.manufacturer;
    }
    
    public double getVolume(){
        return this.volume;
    }
    
    public double getPrice(){
        return this.price;
    }
    
    //Overriding toString() so that GUI can print each list item name
    @Override
    public String toString(){
        return this.name + " " + this.volume + "l " + this.price + "€";
    }
}
