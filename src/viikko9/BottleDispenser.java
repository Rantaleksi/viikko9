/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author Aleksi
 */
public class BottleDispenser {
    
    private int bottles;
    private double money;
    private String receipt;
    
    
    private ArrayList<Bottle> bottle_array = new ArrayList<Bottle>(); // Array for Bottle-objects
    
    static private BottleDispenser bd = null;
    
    // Made private to only allow access through getInstance()
    private BottleDispenser(){ 
        bottles = 6;
        money = 0;
        
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero","Coca-Cola", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero","Coca-Cola", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero","Coca-Cola", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero","Coca-Cola", 0.5, 1.95));
    }

    public double getMoney() {
        return money;
    }
    
    static public BottleDispenser getInstance(){
        if(bd == null){
            bd = new BottleDispenser();
        }
        
        return bd;
    }
    
    public String addMoney(double m){
        money += m;
        return "Klink! Lisää rahaa laitteeseen!";
    }
    
    public String buyBottle(int no){
        
        //Check if array still has bottles, and if money is sufficient
        if(bottles > 0 && money > bottle_array.get(no).getPrice()){
            Bottle bottle = bottle_array.get(no);
            
            //Reduce purchased bottle price from money
            money -= bottle.getPrice();
            
            //Store bottle name for console print
            String bottleName = bottle.getName();
            
            //Store bottle info for receipt
            receipt = bottle.getName() + " " + bottle.getVolume() + "l " + bottle.getPrice() + "€";
            
            //Remove bottle from ArrayList
            this.removeBottle(no);
            
            return "KACHUNK! "+ bottleName + " tipahti masiinasta!";
        }
        //If money is insufficient, print error message
        else if(bottles > 0 && money < bottle_array.get(no).getPrice()){
            return "Syötä rahaa ensin!";
        }
        //If no bottles are left, print error message
        else{
            return "Pullot loppuivat!";
        }
    }
    
    public void returnMoney(){
        money = 0;
    }
    
    public void removeBottle(int i){
        bottle_array.remove(i);
        bottles -= 1;
    }
    
    public ArrayList getArray(){
        return bottle_array;
    }
    
    public void printReceipt() throws IOException{
        ReadAndWriteIO rawio = new ReadAndWriteIO("receipt.txt");
        rawio.writeText(receipt);
    }
}
